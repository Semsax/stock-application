'use strict';
const AWS = require('aws-sdk');

exports.handler = async (event, context) => {
  const documentClient = new AWS.DynamoDB.DocumentClient();

  console.log(event)
  console.log(context)

  const { key } = JSON.parse(event.body);

  let responseBody = "";
  let statusCode = 0;

  const params = {
    ExpressionAttributeNames: { 
      '#sku_no': 'sku_no',
      '#part_number': 'part_number',
      '#sku_description' : 'sku_description',
      '#sku_name' : 'sku_name',
      '#brand' : 'brand'
    },
    ExpressionAttributeValues: {
      ':key': key
    },
    FilterExpression: '#sku_no = :key OR #brand = :key OR #part_number = :key OR contains (#sku_name,:key) OR contains (#sku_description, :key)',
    TableName: 'Products'
  };

  try {
    const data = await documentClient.scan(params).promise();
    responseBody = JSON.stringify(data.Items);
    statusCode = 200;
    console.log(data)
  } catch(err) {
    responseBody = `Unable to get products: ${err}`;
    statusCode = 403;
  }

  const response = {
    statusCode: statusCode,
    headers: {
      "Content-Type": "application/json"
    },
    body: responseBody
  };

  return response;
};