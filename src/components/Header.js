import React, { Component } from 'react'
import { Navbar, Nav, FormControl, Button, InputGroup, Dropdown} from 'react-bootstrap'
import { Auth } from 'aws-amplify'
import { logout } from "../utils/auth"
import { navigate } from 'gatsby'
import axios from 'axios'
import config from '../config.json'

/**
 * Header, a component that will host the title, the login buttons, 
 * logout, return to the list of products and the product search bar, 
 * is present on all pages of the application.
 */
export default class Header extends Component {
    constructor(props) {
        super(props)
        
        this.state = { 
            /** 
             * displayLink: Status that tells the Header what 
             * controls to render.
             */
            displayLink: props.mode,
            /**
             * keySearch: State that storage a string that will 
             * be used to search in the database.
             */
            keySearch: ''
        }
        
        //Binding methods to the component.
        this.handleClickSignIn = this.handleClickSignIn.bind(this)
        this.handleClickSignUp = this.handleClickSignUp.bind(this)    
        this.handleClickReturn = this.handleClickReturn.bind(this)
        this.changeContent = this.changeContent.bind(this)
        this.handleKeyPress = this.handleKeyPress.bind(this)
        this.searchProduct = this.searchProduct.bind(this)
        this.handleGoBack = this.handleGoBack.bind(this)
    }

    /**
     * Method that is triggered by a click and set the property SignIn so the index 
     * changes the content to SignUp
     */
    handleClickSignIn () {
        this.props.signIn("SignUp")
    }

    /**
     * Method that triggers by a click and set the property signUp so the index
     * changes the content to SignIn.
     */
    handleClickSignUp () {
        this.props.signUp("SignIn")
    }

    /**
     * Method that triggers by a click and set the property back so the index
     * changes the content to Dashboard.
     */
    handleClickReturn () {
        this.props.back("Dashboard")
    }

    /**
     * Method that is activated by clicking on the session closing control, 
     * this will close the session with cognito and redirect at the beginning 
     * of the session.
     */
    handleClickLogOut = async event => {
        event.preventDefault()
        Auth.signOut().then(
            logout(()=> 
                navigate('/')
            )
        ).catch(error => 
            console.log(error)
        )
    }

    /**
     * Method that changes the header content depending on the current content 
     * on display.
     */
    changeContent(content) {
        this.setState({displayLink: content})
    }

    /**
     * Method that filters the characters entered in the control, this will 
     * update the status in case it is not enter, when is enter 
     * the product search begins.
     * @param {*} event Parameter that indicates what was typed and where the click comes from
     */
    handleKeyPress(event) {
        if(event.key === 'Enter'){
            this.searchProduct()
        } else {
            this.setState({[event.target.name]: event.target.value})
        }
    }

    /**
     * Method that returns to the login screen when is triggeres.
     */
    handleGoBack() {
        navigate('/')
    }

    /**
     * Asynchronous method that triggers a post to the server to initiate 
     * a search in the database, this will return an array of products. In case 
     * the server returns an error, an empty array will be assigned.
     */
    async searchProduct(){
        try {
            const res = await axios.post(`${config.api.invokeUrl}/scan`,{ key : this.state.keySearch })
            this.props.products(res.data)
        } catch (err) {
            this.props.products([])
            console.log(`An error has occurred: ${err}`)
        }
    }

    /**
     * Method that return a  JSX component according to what will be shown 
     * to the client screen.
     */
    displayMode () {
        switch(this.state.displayLink){
            case "SignIn":
                return (<Nav.Link onClick={this.handleClickSignIn}>Sign Up</Nav.Link>) 
            case "SignUp":
                return (<Nav.Link onClick={this.handleClickSignUp}>Sign In</Nav.Link>)
            case "Dashboard":
                return (
                    <InputGroup className="mb-1">
                        <FormControl name="keySearch" placeholder="Search" onChange={this.handleKeyPress}/>
                        <InputGroup.Append>
                            <Button variant="secondary" onClick={this.searchProduct}>Search</Button>
                        </InputGroup.Append>
                    </InputGroup>
                )
            case "ProductCard":
                return(<Nav.Link onClick={this.handleClickReturn}>Return</Nav.Link>)
            case "Confirmation":
                return (<Nav.Link onClick={this.handleGoBack}>Go back to Sign in</Nav.Link>)
            default: 
                return(<div></div>)
        }
    }

    /**
     * Method that selects what control to put on display in the title section 
     * depending on the displayLink state.
     */
    logOutNav (){
        console.log('Se activo el metodo logOutNav')
        if(this.state.displayLink === "Dashboard"){
            return(<Dropdown className="mr-1">
                        <Dropdown.Toggle variant="primary" id="dropdown-basic" size="lg">
                        Stock Manager 
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                        <Dropdown.Item onClick={this.handleClickLogOut}>Log Out</Dropdown.Item>
                        </Dropdown.Menu>
                </Dropdown>
            )
        } else {
            return(<Navbar.Brand >Stock Manager</Navbar.Brand>)
        }
    }

    /**
     * Method that renders the component.
     */
    render() {
        return (
                <Navbar bg="primary" variant="dark">
                    { this.logOutNav() }
                    <Nav className="ml-auto pr-0">
                        { this.displayMode() }
                    </Nav> 
                </Navbar>
        )  
    }
}