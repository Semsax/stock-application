import React, { Component } from 'react'
import ProductItem from './ProductItem'
import { Pagination, Row, Col, Alert } from 'react-bootstrap'

/**
 * Component that list the products queried in the database, 
 * it will paginate the product list in case is necessary.
 */
export default class Products extends Component { 
    constructor(props){
        super(props)
        this.state = {
            /**
             * State that storage the current page.
             */
            currentPage: this.props.activePage,
            /**
             * State that store the number of products per page.
             */
            productsPerPage: 6,
            /**
             * State that store all products that were found.
             */
            products: this.props.products,
            /**
             * State that verify if is the first time that the 
             * user open the dashboard with the purpose of displaying 
             * the proper message.
             */
            recentlyOpen: true
        }

        //Binding methods to the component.
        this.loopProductsArray = this.loopProductsArray.bind(this)
        this.onClickProductCard = this.onClickProductCard.bind(this)
        this.handleClickPage = this.handleClickPage.bind(this)
        this.setProducts = this.setProducts.bind(this)
        this.noProductsMessage = this.noProductsMessage.bind(this)
    }

    /**
     * Method that assign the array of products that were found, 
     * set the stage recentlyOpen to false in case of products 
     * no were found this will display a the proper message.
     * @param {*} products Array of products
     */
    setProducts(products){
        this.setState({
            products: products,
            recentlyOpen: false
        })
    }
    
    /**
     * Method that will be trigger by a click that came from a page button, 
     * this will change the active page.
     * @param {*} event Event that contains which button was pressed.
     */
    handleClickPage(event){
        this.setState({
            currentPage: Number(event.target.id)
        })
        this.props.pageChange(Number(event.target.id))
    }

    /**
     * This method will be trigger by a click on the product card, 
     * this will tell the dashboard to switch to the product card component.
     * @param {*} product Product that were clicked.
     */
    onClickProductCard (product) {
        this.props.productList(product)
    }

    /**
     * Method that loop the collection of products and returns them as JSX ProductItem.
     */
    loopProductsArray () {
        return this.state.products.map(
            product => (
                <ProductItem product={product} key={product.id} productFromList = {this.onClickProductCard} />
            )
        )
    }

    /**
     * Method that will display two kind of message, the first will be when is the 
     * first opening and the second one will be when no products were found.
     */
    noProductsMessage(){
        if(this.state.products.length === 0) {
            if(this.state.recentlyOpen){ 
                return (
                    <Row className="justify-content-center m-4" >
                        <Col xs="my-1"> 
                            <Alert variant='primary'>Start a product search</Alert>
                        </Col>
                    </Row>
                )
            } else {
                return (
                    <Row className="justify-content-center m-4" >
                        <Col xs="my-1"> 
                            <Alert variant='warning'>Products not found</Alert>
                        </Col>
                    </Row>
                )
            }
        } else {
            return (<div></div>)
        }  
    }

    /**
     * Method that renders the component and manage the logic for pagination.
     */
    render() {   
        const products = this.state.products
        const {currentPage, productsPerPage} = this.state
        
        // Logic for displaying products
        const indexOfLastProduct = currentPage * productsPerPage
        const indexOfFirstProduct = indexOfLastProduct - productsPerPage
        const currentProducts = products.slice(indexOfFirstProduct, indexOfLastProduct)

        const renderProducts = currentProducts.map((product) => {
            return <ProductItem product={product} key={product.sku_no} productFromList = {this.onClickProductCard} />
        })

        // Logic for displaying page numbers
        const pageNumbers = []
        for(let i = 1; i <= Math.ceil(products.length/productsPerPage); i++){
            pageNumbers.push(i)
        }
        
        const renderPageNumbers = pageNumbers.map(number => {
            return(
            <Pagination.Item 
                key={number}
                id={number}
                onClick={this.handleClickPage}
                active={number === currentPage}
            >
                {number}
            </Pagination.Item>
            )
        })

        console.log(this.state.products)

        return (
            <div>
                {this.noProductsMessage()}
                {renderProducts}
                <Row className="justify-content-center my-1" >
                    <Col xs="my-1"> 
                        <Pagination>
                            {renderPageNumbers}
                        </Pagination>
                    </Col>
                </Row>
            </div>
        )
    }
}