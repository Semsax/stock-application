import React, { Component } from 'react'
import { Row, Col , Image, Container, Table } from 'react-bootstrap'

/**
 * Component that will show the product information in more detail.
 */
export default class ProductCard extends Component {     
    /**
     * Method that renders the component.
     */
    render() {
        return(
            <Container>
                <Row>
                    <Col>
                        <Image src={this.props.product.image_reference} alt='Product image' width='300rem' rounded className='rounded mx-auto d-block my-3'/>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h2 className='text-center'>{this.props.product.sku_name}</h2>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h6 className='text-center'>{this.props.product.sku_description}</h6>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Table striped bordered hover>
                                <tbody>
                                    <tr>
                                        <th>SKU number</th>
                                        <td>{this.props.product.sku_no}</td>
                                    </tr>
                                    <tr>
                                        <th>Part Number</th>
                                        <td>{this.props.product.part_number}</td>
                                    </tr>
                                    <tr>
                                        <th>Brand</th>
                                        <td>{this.props.product.brand}</td>
                                    </tr>
                                    <tr>
                                        <th>Vendor cost</th>
                                        <td>{this.props.product.vendor_cost}</td>
                                    </tr>
                                    <tr>
                                        <th>Received Quantity</th>
                                        <td>{this.props.product.received_qty}</td>
                                    </tr>
                                    <tr>
                                        <th>Received Amount</th>
                                        <td>{this.props.product.received_amount}</td>
                                    </tr>
                                    <tr>
                                        <th>Sold Quantity</th>
                                        <td>{this.props.product.qty_sold}</td>
                                    </tr>
                                </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
        )
    }
}