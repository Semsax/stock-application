import React, { Component, createRef } from 'react'
import { Form, Button, Col, Row} from 'react-bootstrap'
import { Auth } from 'aws-amplify'
import { navigate } from 'gatsby'

var passwordValidator = require('password-validator')
var emailValidator = require('email-validator')

/**
 * This component will handle the sign in process.
 */
export default class SignUp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            /**
             * State that store the user email.
             */
            email: '',
            /**
             * State that store the user password.
             */
            password: '',
            /**
             * State that store the user password for confirmation.
             */
            passwordConfirmation: '',
            /**
             * 
             */
            errorServerSide: ''
        }

        /**
         * Creation of references for focus control.
         */
        this.emailInput = createRef()
        this.passwordInput = createRef()
        this.passwordMatchInput = createRef()
    }    
    
    /**
     * Method that assign the input user to the correspondent state.
     */
    handleInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    /**
     * Method that verify the user password.
     */
    passwordMatch(){
        return this.state.password === this.state.passwordConfirmation
    }

    /**
     * Async method that send the user information to the server for Signup.
     */
    handleSubmit = async event => {
        event.preventDefault()

        if(!this.isAnEmail()) {
            this.emailInput.current.focus()
            return
        }
        if(!this.isPasswordValid()) { 
            this.passwordInput.current.focus()
            return
        }
        if(!this.passwordMatch()){
            this.passwordMatchInput.current.focus()
            return
        }

        const { email, password } = this.state
        const username = email
        try {
            //const signUpResponse = await Auth.signUp({
            //    username,password
            //})
            await Auth.signUp({username,password})
            navigate('/confirmation')
        } catch(error) {
            this.errorHandler(error)
        }
    }

    /**
     * Method that verify the email.
     */
    isAnEmail(){
        return emailValidator.validate(this.state.email)
    }

    /**
     * Method that verify the password requisites.
     */
    isPasswordValid(){
        return new passwordValidator()
        .is().min(8)
        .is().max(16)
        .has().uppercase()
        .has().lowercase()
        .has().digits()
        .has().not().spaces()
        .validate(this.state.password) 
    }

    /**
     * Method that handle errors occurred on the signup process.
     * @param {*} error Error ocurred.
     */
    errorHandler(error){
        switch(error.code){
            case "UsernameExistsException":
                this.setState({ errorServerSide: 'This email is already registered.'})
                this.emailInput.current.focus()
            break
            default:
                this.setState({ errorServerSide: 'Something went wrong, contact the administrator.'})
            break
        }
    }

    /**
     * Method that renders the component.
     */
    render() {
        return (
            <Form className="border border-primary m-5 p-2 rounded" onSubmit={this.handleSubmit}>
                <Row className="text-center">
                    <Col md={{ span: 6, offset: 3 }}>
                        <h1 variant="primary">Register</h1>
                    </Col>
                </Row>
                <Form.Row>
                    <Form.Group as={Col} controlId="formGridEmail" md={{span: 4, offset: 4}}>
                    <Form.Label >Email</Form.Label>
                    <Form.Control name='email' ref={this.emailInput} type="email" placeholder="Enter email" onInput={this.handleInput} required/>
                    {this.isAnEmail() ?
                    <Form.Label className="text-muted">Email Ok</Form.Label> : 
                    <Form.Label className="text-muted">Type a valid email</Form.Label>}
                    </Form.Group>
                </Form.Row>
                <Form.Row>
                    <Form.Group as={Col} controlId="formGridPassword" md={{span: 4, offset: 4}}>
                    <Form.Label>Password</Form.Label>
                    <Form.Control name='password' ref={this.passwordInput} type="password" placeholder="Password" onChange={this.handleInput} required/>
                    {this.isPasswordValid() ?
                        (<Form.Label className="text-muted">Password Ok</Form.Label>) :
                        (<Form.Label className="text-muted">Password must be 8 characters minimum, include an uppercase, a digit, no spaces.</Form.Label>)
                    }
                    </Form.Group>
                </Form.Row>
                <Form.Row>
                    <Form.Group as={Col} controlId="formGridPasswordConfirmation" md={{span: 4, offset: 4}}>
                    <Form.Label>Password confirmation</Form.Label>
                    <Form.Control name='passwordConfirmation' ref={this.passwordMatchInput} type="password" placeholder="Password" onChange={this.handleInput} required/>
                    {this.passwordMatch() ?
                        (<Form.Label className="text-muted">Password match</Form.Label>) :
                        (<Form.Label className="text-muted">Password does not match</Form.Label>)
                    }
                    </Form.Group>
                </Form.Row>
                <Form.Row className="text-center">
                    <Col md={{span: 2, offset: 5}} xs={{span: 8, offset: 2}}>
                        <Button variant="outline-primary" type="submit">
                            Sign Up
                        </Button>
                    </Col>
                </Form.Row>
                <Form.Row className="text-center my-1">
                    <Col md={{span: 2, offset: 5}} xs={{span: 8, offset: 2}}>
                        <Form.Label className="text-danger">{this.state.errorServerSide}</Form.Label>
                    </Col>
                </Form.Row>
            </Form>
        )
    }
}