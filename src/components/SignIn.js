import React, { Component } from 'react';
import { Form, Col, Button , Row} from 'react-bootstrap';
import { Auth } from 'aws-amplify';
import { setUser, isLoggedIn } from '../utils/auth'
import { navigate } from 'gatsby'

/**
 * This component will handle the sign in process.
 */
export default class SignIn extends Component { 
    constructor(props){
        super(props)
        this.state = {
            /**
             * State that store the user email
             */
            email: '',
            /**
             * State that store the user password
             */
            password: '',
            /**
             * State that store an error got from the server side.
             */
            errorServerSide: ''
        }
    }

    /**
     * Async method that send the user information to the server for log in.
     */
    handleSubmit = async event => {
        event.preventDefault()
        const {email,password} = this.state
        try{
            await Auth.signIn(email,password)
            const user = await Auth.currentAuthenticatedUser()
            const userInfo = {
                ...user.attributes,
                username: user.username
            }
            setUser(userInfo)
            navigate('/dashboard/')
        }
        catch(error) {
           this.errorHandler(error)
        }
    }

    /**
     * Method that will display and error message in case of.
     * @param {*} error Error received.
     */
    errorHandler(error){
        console.log(error)
        switch(error.code){
            case "NotAuthorizedException":
                this.setState({ errorServerSide: 'Incorrect username or password.'})
            break
            case "UserNotConfirmedException":
                this.setState({ errorServerSide: 'The email provided must be confirmed before get access, verify your mailbox.'})
            break
            default:
                this.setState({ errorServerSide: 'Something went wrong, contact the administrator.'})
            break
        }
    }

    /**
     * Method that assign the input user to the correspondent state.
     */
    handleInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }


    /**
     * Method that renders the component.
     */
    render() {
        if(isLoggedIn()) navigate('/dashboard/')
        return (
            <Form className="border border-primary m-5 p-2 rounded" onSubmit={this.handleSubmit}>
                <Row className="text-center">
                    <Col md={{ span: 6, offset: 3 }}>
                        <h1 variant="primary">Sign in</h1>
                    </Col>
                </Row>
                <Form.Row>
                    <Form.Group as={Col} controlId="formGridEmail" md={{span: 4, offset: 4}}>
                    <Form.Label >Email</Form.Label>
                    <Form.Control name='email' type="email" placeholder="Enter email" onInput={this.handleInput} required/>
                    </Form.Group>
                </Form.Row>
                <Form.Row>
                    <Form.Group as={Col} controlId="formGridPassword" md={{span: 4, offset: 4}}>
                    <Form.Label>Password</Form.Label>
                    <Form.Control name='password' type="password" placeholder="Password" onInput={this.handleInput} required/>
                    </Form.Group>
                </Form.Row>
                <Form.Row className="text-center">
                    <Col md={{span: 2, offset: 5}} xs={{span: 8, offset: 2}}>
                        <Button variant="outline-primary" type="submit">
                            Sign In
                        </Button>
                    </Col>
                </Form.Row><Form.Row className="text-center my-1">
                    <Col md={{span: 2, offset: 5}} xs={{span: 8, offset: 2}}>
                        <Form.Label className="text-danger">{this.state.errorServerSide}</Form.Label>
                    </Col>
                </Form.Row>
            </Form>
        )
    }
}