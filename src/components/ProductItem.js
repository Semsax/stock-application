import React, { Component } from 'react'
import { Row, Col , Image, Alert } from 'react-bootstrap'

/**
 * A component that will be displayed as an item in the product list when a search is made to 
 * return the products.
 */
export default class ProductItem extends Component { 
    constructor(props) {
        super(props)


        this.state = { 
            /**
             * product: State that temporarily stores the selected product for a more detailed look.
             */
            product : props.product,
            /**
             * variant: State that will return the type of alert depending on the stock of the product.
             */
            variant : props.product.received_qty - props.product.qty_sold > 0 ? 'success' : 'danger',
            /**
             * quantity: Status that storage the quantity of the product in stock
             */
            quantity: props.product.received_qty - props.product.qty_sold
         }

        //Binding methods to the component.
        this.handleClickProduct = this.handleClickProduct.bind(this)
    }

    /*
    * Method that returns the product to the parent for displaying the product card
    */
    handleClickProduct () {
        this.props.productFromList(this.state.product)
    }

    /** 
    * Method that renders the component.
    */
    render() {
        return(
            <div>
                <Row className="border-bottom p-0">
                    <Col className="ml-2 mt-3">
                        <Image src={this.state.product.image_reference} alt="Product image" width="100px" rounded onClick={this.handleClickProduct} style={{cursor: 'pointer'}}/>
                    </Col>
                    <Col>
                        <p className="text-muted mb-0" style={{cursor: 'pointer'}} onClick={this.handleClickProduct}><small>{this.state.product.sku_name}</small></p>
                        <p className="text-muted mb-0"><small>{this.state.product.sku_no}</small></p>
                        <p className="text-dark text-center">MEX {this.state.product.sale_price}</p>
                    </Col>
                    <Col>
                        <Alert variant={this.state.variant} className='p-3 mt-4 mx-3 text-center'>{this.state.quantity}</Alert>
                    </Col>
                </Row>
            </div>
        )
    }
}