import React, { Component, createRef } from "react"
import 'bootstrap/dist/css/bootstrap.min.css'
import ProductCard from "../components/ProductCard"
import Products from "../components/Products"
import Header from "../components/Header"
import { isLoggedIn } from '../utils/auth'
import { navigate } from "gatsby"

/**
 * Page that shows the navigation screen to search for products and 
 * to see their respective cards
 */
export default class dashboard extends Component {
    constructor(props){
        super(props)
        this.state = {
            /**
             * State that indicates to header which content display.
             */
            dislayContent: "Dashboard",
            /**
             * State that store the product to display
             */
            productToDisplay: undefined,
            /**
             * State that store the current page
             */
            activePage: 1,
            /**
             * State that store the collection of products
             */
            products: []
        }
        /**
         * References for products and content on display
         */
        this.productsRef = createRef()
        this.navContent = createRef()
    }

    /**
     * Method that is activated when the user clicks on the product 
     * image in the product list. This will assign the productToDisplay 
     * status to the product that was clicked and set the displayProductCard flag to 
     * true to show the ProductCard components with the selected product.
     */
    onClickProductCard = (product) => {
        this.setState({
            productToDisplay: product,
            dislayContent: "ProductCard"
        })
        this.navContent.current.changeContent("ProductCard")
    }

    /**
     * Method that changes the product that is being displayed to undefined, 
     * the content variable and changes the content of the header to display the search bar.
     */
    onClickReturn = (back) => {
        this.setState({
            productToDisplay: undefined,
            displayContent: back
        })
        this.navContent.current.changeContent(back)
    }

    /**
     * Method that set the active page from the component Products.
     */
    changeActivePage = (page) => {
        this.setState({
            activePage: page
        })
    }

    /**
     * Method that get the products from the header and send them to Products Component.
     */
    getProducts = (products) => {
        this.setState({products: products})
        this.productsRef.current.setProducts(products)
    }

    /**
     * Method that redirect to the Signin screen if the user is not logged.
     */
    componentDidMount(){
        if(!isLoggedIn()) navigate('/')
    }

     /**
     * Method that renders the component.
     */
    render () {
        return (
            <div>
                <Header 
                    mode={this.state.dislayContent} 
                    back={this.onClickReturn} 
                    ref={this.navContent}
                    products={this.getProducts}
                />
                {
                this.state.productToDisplay !== undefined ?
                    (<ProductCard product={this.state.productToDisplay}/>) :
                    (<Products 
                        productList={this.onClickProductCard} 
                        activePage={this.state.activePage}
                        pageChange={this.changeActivePage}
                        products={this.state.products}
                        ref={this.productsRef}
                    />)
                }
            </div>
        )
    }
}