import React, { Component } from "react"

/**
 * 404 Page not found.
 */
export default class index extends Component {
     /**
     * Method that renders the component.
     */
    render (){
        return (
            <div>
                <h1>404 Page not found</h1>
            </div>   
        )
    }
}