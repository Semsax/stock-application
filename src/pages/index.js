import React, { Component, createRef} from "react"
import 'bootstrap/dist/css/bootstrap.min.css'
import SignIn from "../components/SignIn"
import SignUp from "../components/SignUp"
import Header from "../components/Header"
import Amplify from "aws-amplify"
import config from "../config"

/**
 * Configuration for AWS cognito 
 */
Amplify.configure({
    Auth: {
        mandatorySignId: true,
        region: config.cognito.REGION,
        userPoolId: config.cognito.USER_POOL_ID,
        userPoolWebClientId: config.cognito.APP_CLIENT_ID
    }
})

/**
 * Page that will handle the 
 */
export default class index extends Component {
    /**
     * displayContent: Estado que indicara que se 
     * pondra en display, signin o signup
     */
    state = { displayContent: "SignIn" }

    /**
     * Reference for display the content
     */
    navContent = createRef()
    
    /**
     * Method that display the signIn content
     */
    onClickSignIn = (signIn) => {
        this.setState({ displayContent: signIn })
        this.navContent.current.changeContent(signIn)
    }

    /**
     * Method that display the signUp content
     */
    onClickSignUp = (signUp) => {
        this.setState({ displayContent: signUp })
        this.navContent.current.changeContent(signUp)
    }

     /**
     * Method that renders the component.
     */
    render (){
        return (
            <div>
                <Header 
                    mode={this.state.displayContent} 
                    signIn={this.onClickSignIn}
                    signUp={this.onClickSignUp}
                    ref = {this.navContent}
                />
                {
                    this.state.displayContent === "SignIn" ? 
                        (<SignIn/>) : 
                        (<SignUp/>)
                }
            </div>   
        )
    }
}
