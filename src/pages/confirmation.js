import React, { Component } from "react"
import { Alert } from 'react-bootstrap'
import Header from "../components/Header"

/**
 * This page will be displayed after the signup finish.
 */
export default class confirmation extends Component {
    state = { displayContent : "Confirmation"}

    /**
     * Method that renders the component.
     */
    render (){
        return (
            <div>
                <Header mode={this.state.displayContent} />
                <Alert variant="success">
                    <Alert.Heading>A confirmation email has been sended to the email provided.</Alert.Heading>
                    <p>The email provides a confirmation link, you must confirm before get access to the application.</p>
                </Alert>
            </div>   
        )
    }
}
